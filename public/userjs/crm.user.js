// ==UserScript==
// @name         CRM-Chrome-Mods
// @namespace    tmg@cbord.com
// @version      2.1.1
// @description  Hacks to make CRM work in Chrome
// @author       tmg@cbord.com
// @include      https://cbord.tribridgecloud.com/*
// @grant        none
// @run-at       document-idle
// @downloadURL  https://renshep.gitlab.io/userjs/crm.user.js
// ==/UserScript==

        console.log('CRM-Chrome-Mods Start: ' + document.location.href);

    // change alerts into notifications
        window.myNotif = window.Notification;
        //console.log(window.myNotif);
        window.realAlert = window.alert;
        //console.log(window.realAlert);
        window.alert = function(alertText) {
            //console.log(alertText);
            if (alertText.startsWith("There was an error")) return;
            window.myNotif.requestPermission(
                function() {
                    var newNotif = new window.myNotif('CRM Notification',{body:alertText});
                    newNotif.onclick = function() {
                        this.close(); // close the notification when clicked
                    };
                });
        };
        //console.log(window.alert);

    // enable damn location product lookup button
        var damnbutton = false;
        damnbutton = document.getElementById('neudesic_partid');
        if (damnbutton) damnbutton.setAttribute('lookupdisabled',"false");

    // enable copy of ticket number and title
        var enableme = false;
        enableme = document.getElementById('ticketnumber');
        if (enableme) enableme.disabled = false;
        enableme = document.getElementById('title');
        if (enableme) enableme.disabled = false;

    // changes notes to monospace font
        //GM_addStyle('.editArea, #noteTextArea { font-family: monospace; }');
        var newCSS = document.createElement('style');
        newCSS.textContent = '';
        newCSS.textContent += ' .editArea, #noteTextArea { font-family: monospace; min-width: 1000px; } ';
        newCSS.textContent += ' textarea { min-height: 500px; } ';
        newCSS.textContent += ' #neudesic_urgent { min-height: auto; } ';
        newCSS.textContent += ' #WebResource_notes { resize: both; overflow: auto; }  ';
        document.head.appendChild(newCSS);

    // monkey patch window.open so it opens tabs instead of new windows
        window.origOpen = window.open;
        window.open = function(a,b) {
            return window.origOpen(a,b);
        };

    // monkey patch out window.onbeforeunload
        window.orig_addEventListener = window.addEventListener;
        window.addEventListener = function(a) {
            if (a == "onbeforeunload") return;
            window.orig_addEventListener.apply(this,arguments);
        };


    // ADD MORE MODS HERE

        console.log('CRM-Chrome-Mods Complete: ' + document.location.href);

